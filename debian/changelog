fastjet (3.0.6+dfsg-4) UNRELEASED; urgency=medium

  * Trim trailing whitespace.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 20 Oct 2018 20:47:43 +0000

fastjet (3.0.6+dfsg-3) unstable; urgency=medium

  * Team upload
  * Fix FTBFS of architecture-dependant builds.  Closes: #827252
  * Allow `make install` to run parallel

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 14 Jun 2016 09:02:49 +0000

fastjet (3.0.6+dfsg-2) unstable; urgency=medium

  * Team upload.
  * add patch to gain reproducible build.  Closes: #787865
  * run wrap-and-sort -ast.
  * bump debhelper compat level to 9.
  * d/rules: remove useless variables.
  * remove root-macro-fastjet binary package.
    ROOT is being removed from Debian.
  * canonicalize Vcs-* fields.
  * bump Standards-Version to 3.9.8, no changes needed.
  * remove temporary doxygen files from the built package.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 13 Jun 2016 19:39:39 +0000

fastjet (3.0.6+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename package for GCC 5 transition. (Closes: #797989)

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 19 Oct 2015 23:02:48 +0200

fastjet (3.0.6+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.4.
  * Remove obsolte DM-Upload-Allowed field.
  * Canonical VCS-* fields.
  * libfastjetplugins links against gfortran. (Closes: #728156)

 -- Lifeng Sun <lifongsun@gmail.com>  Sun, 15 Dec 2013 18:42:11 +0800

fastjet (3.0.3+dfsg-1) unstable; urgency=low

  * Upload to sid.

 -- Lifeng Sun <lifongsun@gmail.com>  Mon, 06 May 2013 12:08:50 +0800

fastjet (3.0.3+dfsg-1~exp1) experimental; urgency=low

  * New upstream release.
  * Remove patch missed-this-pointer.patch: applied by upstream.
  * debian/control: move doxygen-latex and graphviz from Build-Depends to
    Build-Depends-Indep.
  * libfastjetplugins: links against siscone libraries.
  * Update dependencies of binary packages.
  * debian/get-orig-source: support uscan --destdir option. (Closes: #676983)
  * debian/rules: change to tiny-style.

 -- Lifeng Sun <lifongsun@gmail.com>  Tue, 03 Jul 2012 07:37:02 +0800

fastjet (3.0.2+dfsg-2) unstable; urgency=low

  * Fix dependencies of libfastjet-fortran-dev.
  * Disable rpath flag in fastjet-config by default.

 -- Lifeng Sun <lifongsun@gmail.com>  Mon, 04 Jun 2012 08:33:14 +0800

fastjet (3.0.2+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #636979)

 -- Lifeng Sun <lifongsun@gmail.com>  Wed, 23 May 2012 19:11:32 +0800
